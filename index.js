console.log("Hello World");

const firstName = "Naz", lastName = "Glang", age = "19";
let hobbies = ["Eating", "Playing Mobile Games"];

let workAddress = {
	houseNumber: "001",
	street: "Tiano-Yacapin",
	city: "Cag de Oro City",
	state: "Philippines",
}

function printUserInfo() {
    console.log("First Name: " +firstName);
    console.log("Last Name: " +lastName);
    console.log("Age: " +age);
    console.log("Hobbies:");
    console.log(hobbies);
    console.log("Work Address:");
    console.log(workAddress);
};


printUserInfo(firstName, lastName, age, hobbies, workAddress);

function returnFunction() {
    return  firstName + lastName + age + hobbies + workAddress;
}; 

console.log(lastName + " " + firstName + " is " + age  + " years of age." );
console.log("This is printed inside of the function.")
console.log(hobbies);
console.log("This is printed inside of the function.")
console.log(workAddress);

let isMarried = true;
console.log("The value of isMarried is: " +isMarried);